package com.example.demo2appjwt.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {
    private T data;
    private String message;
    private Integer code;
    private Boolean status;
}

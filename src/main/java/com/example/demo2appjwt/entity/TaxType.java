package com.example.demo2appjwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * soliq turlari  gaz , elektr..
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tax_type")
public class TaxType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "tax_name")
    private String taxName;
    @Column(name = "code",nullable = false)
    private String code;

}

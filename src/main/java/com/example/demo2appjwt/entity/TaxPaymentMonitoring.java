package com.example.demo2appjwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Soliqdagilar uchun tolovchining malumotlari
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tax_monitor")
public class TaxPaymentMonitoring {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(max = 9)
    @Column(name = "user_tin", nullable = false)
    private String tin;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tax_type_id")
    private TaxType taxType;
    @Column(name = "funds")
    private Integer summa;
    @Column(name = "date_of_pay")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "tax_code",nullable = false)
    private String code;

}

package com.example.demo2appjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Demo2AppJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo2AppJwtApplication.class, args);
    }

}

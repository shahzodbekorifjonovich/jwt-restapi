package com.example.demo2appjwt.repository;


import com.example.demo2appjwt.entity.TaxPaymentMonitoring;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxMonitorRepository extends JpaRepository<TaxPaymentMonitoring,Integer> {
}

package com.example.demo2appjwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//    @Column(nullable = false)
//    private String name;
//    @Column(nullable = false, unique = true)
//    private String username;
//    @Column(nullable = false,unique = true)
//    @Email
//    private String email;
//    @Column(nullable = false)
//    private String password;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    @Size(max = 9)
    @Column(name = "tin", nullable = false)
    private String tin;
    @Size(max = 250)
    @Column(nullable = false)
    private String username;
    @Column(nullable = false,unique = true)
    @Email
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private Integer summa;

    public User(@Size(max = 9) String tin, @Size(max = 250) String username, @Email String email, String password, Integer summa) {
        this.tin = tin;
        this.username = username;
        this.email = email;
        this.password = password;
        this.summa = summa;
    }

    //    public User(String name, String username, String email, String encode) {
//        this.name = name;
//        this.username = username;
//        this.email = email;
//        this.password = encode;
//    }


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;


}

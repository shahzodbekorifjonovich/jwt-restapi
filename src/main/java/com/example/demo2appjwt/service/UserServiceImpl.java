package com.example.demo2appjwt.service;

import com.example.demo2appjwt.dto.UserDto;
import com.example.demo2appjwt.dto.UserPaymentDto;
import com.example.demo2appjwt.entity.TaxPaymentMonitoring;
import com.example.demo2appjwt.entity.TaxType;
import com.example.demo2appjwt.entity.User;
import com.example.demo2appjwt.entity.UserPayment;
import com.example.demo2appjwt.model.ResponseData;
import com.example.demo2appjwt.model.Result;
import com.example.demo2appjwt.repository.TaxMonitorRepository;
import com.example.demo2appjwt.repository.TaxTypeRepository;
import com.example.demo2appjwt.repository.UserPaymentRepository;
import com.example.demo2appjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaxTypeRepository taxTypeRepository;

    @Autowired
    private UserPaymentRepository userPaymentRepository;

    @Autowired
    private TaxMonitorRepository taxMonitorRepository;

    @Override
    public UserDto getUser(Integer id) {
        UserDto userDto = new UserDto();
        User user = new User();
        user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("user not found"));
        if (user != null) {
            userDto.setUsername(user.getUsername());
            userDto.setTin(user.getTin());
            userDto.setSumma(user.getSumma());
        } else {
            userDto = null;
        }
        return userDto;
    }

    private Result<User> userChanges(UserPaymentDto userPaymentDto) {
        Result<User> result = new Result<>();
        User user = new User();
        int changeSum = 0;
        try {
            if ((userPaymentDto.getTin().startsWith("5")
                    || userPaymentDto.getTin().startsWith("6"))
                    && userPaymentDto.getTin().length() == 9) {
                user = userRepository.findByTin(userPaymentDto.getTin());
                if (user != null) {
                    changeSum = user.getSumma() - userPaymentDto.getSumma();
                    user.setSumma(changeSum);
                    result.setMessage("save User changes");
                    result.setData(user);
                } else {
                    result.setMessage("Don't exist user in Database");
                    result.setData(null);
                }
            } else {
                result.setMessage("You entered wrong  user tin");
                result.setData(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    @Transactional
    public ResponseData<User> saved(UserPaymentDto userPaymentDto) {
        ResponseData<User> response = new ResponseData<>();
        Result<User> userResult = new Result<>();
        UserPayment userPayment = new UserPayment();
        TaxPaymentMonitoring taxPaymentMonitoring = new TaxPaymentMonitoring();
        User user = new User();
        try {
            userResult = userChanges(userPaymentDto);
            if (userResult.getData() != null) {
                user = userResult.getData();
                TaxType taxType = taxTypeRepository.findByCode(userPaymentDto.getCode());
                if (taxType != null) {
                    userPayment.setUser(user);
                    userPayment.setDate(userPaymentDto.getDate());
                    userPayment.setSumma(userPaymentDto.getSumma());
                    userPayment.setTaxType(taxType);
                    taxPaymentMonitoring.setTin(userPaymentDto.getTin());
                    taxPaymentMonitoring.setCode(userPaymentDto.getCode());
                    taxPaymentMonitoring.setDate(userPaymentDto.getDate());
                    taxPaymentMonitoring.setSumma(userPaymentDto.getSumma());
                    taxPaymentMonitoring.setTaxType(taxType);
                    userRepository.save(user);
                    userPaymentRepository.save(userPayment);
                    taxMonitorRepository.save(taxPaymentMonitoring);
                    userResult.getData().setRoles(null);
                    response.setMessage("Your pay do successfully");
                    response.setStatus(true);
                    response.setCode(200);
                    response.setData(userResult.getData());
                } else {
                    response.setMessage("Error your tax code");
                    response.setStatus(false);
                    response.setCode(400);
                }
            } else {
                response.setMessage(userResult.getMessage());
                response.setStatus(false);
                response.setCode(400);
                response.setData(userResult.getData());
            }


        } catch (Exception e) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return response;
    }

    @Override
    public List<User> getAllUser() {
        List<User> users = new ArrayList<>();
        users = userRepository.findAll();
        return users;
    }
}

package com.example.demo2appjwt.repository;

import com.example.demo2appjwt.entity.Role;
import com.example.demo2appjwt.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer> {
    Optional<Role> findByName(RoleName name);
}

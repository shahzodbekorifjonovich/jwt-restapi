package com.example.demo2appjwt.service;


import com.example.demo2appjwt.dto.UserDto;
import com.example.demo2appjwt.dto.UserPaymentDto;
import com.example.demo2appjwt.entity.User;
import com.example.demo2appjwt.model.ResponseData;

import java.util.List;

public interface UserService {
    UserDto getUser(Integer id);

    ResponseData<User> saved(UserPaymentDto userPaymentDto);

    List<User> getAllUser();
}

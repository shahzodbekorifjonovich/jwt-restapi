package com.example.demo2appjwt.repository;


import com.example.demo2appjwt.entity.UserPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPaymentRepository extends JpaRepository<UserPayment,Integer> {

//    List<UserPayment> findAllByUserId(Integer userId);
//
//    List<UserPayment> findAllById(Integer userId);
}

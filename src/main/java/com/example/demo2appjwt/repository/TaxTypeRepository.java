package com.example.demo2appjwt.repository;


import com.example.demo2appjwt.entity.TaxType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxTypeRepository extends JpaRepository<TaxType,Integer> {
    TaxType findByCode(String code);
}

package com.example.demo2appjwt.config.jwt;

import com.example.demo2appjwt.model.UserPrinciple;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {
    private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${jwt-secret-key}")
    private String jwtSecretKey;
    @Value("${jtw-expire-date}")
    private int expDate;

    public String generateJwtToken(Authentication authentication){
        UserPrinciple userPrinciple =  (UserPrinciple) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(userPrinciple.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + expDate))
                .signWith(SignatureAlgorithm.HS512,jwtSecretKey)
                .compact();
    }

    public String getUserNameFromJwtToken(String token){
        return Jwts.parser()
                .setSigningKey(jwtSecretKey)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken){
        try {
            Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(authToken);
            return true;
        }catch (SignatureException e){
            logger.error("invalid JwtSignature -> Message {} " , e);
        }
        catch (MalformedJwtException e){
            logger.error("invalid Jwt Token -> Message {} ", e);
        }
        catch (ExpiredJwtException e){
            logger.error("Expired Jwt Token -> Message {} ",e);
        }
        catch (UnsupportedJwtException e){
            logger.error("Unsupported JWt Token -> Message{} " , e);
        }
        catch (IllegalArgumentException e){
            logger.error("Jwt claims string is empty -> Message{} " , e);
        }
        return false;
    }

}

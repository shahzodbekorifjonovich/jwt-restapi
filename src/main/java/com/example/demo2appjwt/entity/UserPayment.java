package com.example.demo2appjwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 *userning to'lovlari
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_payment")
public class UserPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tax_type_id")
    private TaxType taxType;                              //to'lov qilingan summa
    @Column(name = "payment_sum",nullable = false)
    private Integer summa;

    @Column(name = "date_of_pay")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
}
